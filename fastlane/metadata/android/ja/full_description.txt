Fedilabはマイクロブログや写真の共有、ビデオのホスティングなどの分散型Fediverseにアクセスするための多機能なAndroidクライアントです。

対応:
Mastodon, Pleroma, Peertube, GNU Social, Friendica


The app has advanced features (especially for Pleroma and Mastodon):

・マルチアカウント対応
・端末からのメッセージの予約
・ブーストの予約
・メッセージをブックマーク
・リモートインスタンスのフォローや通信
・アカウントを時限ミュート
- Cross-account actions with a long press
・翻訳機能
・アートタイムライン
・ビデオタイムライン
