Fedilab és un client Android multifuncional per a l'accés al Fedivers distribuït i que permet micro-bloguejar, compartir fotos i hostatjar vídeos.

Pot incorporar:
- Mastodon, Pleroma, Peertube, GNU social i Friendica.


L'aplicació té funcionalitats avançades (sobretot per a Pleroma i Mastodont):

- Pot assumir múltiples comptes
- Programar missatges des del dispositiu
- Programar difusions d'altres missatges
- Marcar missatges
- Seguir i interactuar amb instàncies remotes
- Silenciar comptes temporalment
- Executar accions entre els comptes amb pitjada llarga
- Funcionalitat de traducció
- Pissarres de creació artística
- Pissarres de vídeo
