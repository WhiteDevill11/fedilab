Fedilab, mikro blog, fotoğraf paylaşımı ve video barındırma içeren dağıtılmış Fediverse'e erişmek için çok işlevli bir Android istemcisidir.

Destekledikleri:
- Mastodon, Pleroma, Peertube, GNU Social, Friendica.


The app has advanced features (especially for Pleroma and Mastodon):

- Çoklu hesap desteği
- Mesajları cihazdan zamanlayın
- Yinelemeleri zamanlayın
- Mesajları yer imlerine ekleyin
- Uzak örnekleri takip edin ve etkileşime geçin
- Hesapları sessize almayı zamanlayın
- Cross-account actions with a long press
- Çeviri özelliği
- Sanatsal zaman çizelgeleri
- Video zaman çizelgeleri
