Fedilab es un cliente multifuncional de Android para acceder al Fediverso distribuido, que permite microblogging, compartición fotos y alojamiento de vídeo.

Es compatible con:
- Mastodon, Pleroma, Peertube, GNU Social, Friendica.


The app has advanced features (especially for Pleroma and Mastodon):

- Soporte de múltiples cuentas
- Programar mensajes desde el dispositivo
- Programar impulsos
- Guardar mensajes
- Seguir e interactuar con instancias remotas
- Silenciar cuentas temporalmente
- Cross-account actions with a long press
- Traducción de estados
- Líneas de tiempo de arte
- Líneas de tiempo de vídeos
