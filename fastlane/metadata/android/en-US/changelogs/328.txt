Added
- Improve battery drain with delayed notifications
- Allow to reverse the list of "Your toots"
- Bottom loader when loading long threads

Fixed
- Interacting with a post resets quick reply
- Timed mute does not mute boosts
- Upload compression for videos badly resized
- Proxy not working when uploading media
- Fix Peertube discover
- Fix notifications displayed twice
- Fix crash with art timeline
